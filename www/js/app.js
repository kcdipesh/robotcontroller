// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic','btford.socket-io'])

//angular.module(_CONFIG_, [])
    .constant('APP_NAME','Robot Controller')
    .constant('APP_VERSION','1.0')
    .constant('PI_URI','http://192.168.100.4:3000')
    //http://192.168.1.109/Hemuppgiften2.0/webservice
    //http://109.74.12.180/Hemuppgiften2.0/webservice

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.directive('enter', function(mySocket) {
  return function (scope, element, attrs) {
    element.bind('touchstart', function() {
       if(attrs.enter) {
          console.log('sent: '+attrs.enter);
          mySocket.emit(attrs.enter)
       }
    })
  }
})

.directive('release', function(mySocket) {
  window.myS= mySocket;
  return function (scope, element, attrs) {
    element.bind('touchend touchcancel', function() {
       
       if(attrs.release) {
          mySocket.emit(attrs.release);
          console.log('sent:'+attrs.release);
       }

    })
  }
})


.directive('controlPad',function(mySocket){
    return function(scope,element,attrs) {
      return;
      //alert('getstarted');
      //
      //send stop signal as soon as a event is fired "touchend"
        //console.log(mySocket);
        //console.log(socketFactory);

      // mySocket.on('okdipesh',function(event,msg) {

      // });  
      //soft stop
      var stopSignal = function(){
        //emit stop signal
        console.log('sending stop signal');
        mySocket.emit("ss");
        scope.$broadcast("ss");


      };

      //hard stop
      var forcedStopSignal = function(){
        //emit stop signal
        console.log('sending forced stop signal');
        mySocket.emit("hs");
      };
      
      var goForward = function(){
        console.log('going forward');
        mySocket.emit("f");

      };

      var goBackward = function(){
        console.log('going backward');

      };
      var goLeft = function(){
        console.log('going left');

      };
      var goRight = function(){
        console.log('going right');

      };


      var goForwardLeft = function(){
        console.log('going forward left');
      };

      var goForwardRight = function(){
        console.log('going forward right');
      };
      var goBackwardLeft = function(){
        console.log('going backward left');
      };
      var goBackwardRight = function(){
        console.log('going backward right');
      };
      




      var buttons;
      var f,l,r,b;
      var fl,fr,bl,br;
      f = document.getElementById('f'), // button: forward
      l = document.getElementById('l'), // button: left
      r = document.getElementById('r'), // button: right
      b = document.getElementById('b'), // button: backward
      
      fl = document.getElementById('fl'), // button: forward-left
      fr = document.getElementById('fr'), // button: forward-right
      bl = document.getElementById('bl'), // button: backward-right
      br = document.getElementById('br'), // button: backward-right

      s = document.getElementById('s') // button: stop  
      ;

      //forward
      f.addEventListener('touchstart',goForward,false);
      
      //left
      l.addEventListener('touchstart',goLeft,false);

      //right
      r.addEventListener('touchstart',goRight,false);

      //backward
      b.addEventListener('touchstart',goBackward,false);


      //forward-left
      fl.addEventListener('touchstart',goForwardLeft,false);

      //forward-right
      fr.addEventListener('touchstart',goForwardRight,false);


      //backward-left
      bl.addEventListener('touchstart',goBackwardLeft,false);

      //backward-right
      br.addEventListener('touchstart',goBackwardRight,false);




      f.addEventListener('touchend',stopSignal, false);
      l.addEventListener('touchend',stopSignal, false);
      r.addEventListener('touchend',stopSignal, false);
      b.addEventListener('touchend',stopSignal, false);
      
      fl.addEventListener('touchend',stopSignal, false);
      fr.addEventListener('touchend',stopSignal, false);
      bl.addEventListener('touchend',stopSignal, false);
      br.addEventListener('touchend',stopSignal, false);


      f.addEventListener('touchcancel',stopSignal, false);
      l.addEventListener('touchcancel',stopSignal, false);
      r.addEventListener('touchcancel',stopSignal, false);
      b.addEventListener('touchcancel',stopSignal, false);      

      fl.addEventListener('touchcancel',stopSignal, false);
      fr.addEventListener('touchcancel',stopSignal, false);
      bl.addEventListener('touchcancel',stopSignal, false);
      br.addEventListener('touchcancel',stopSignal, false);
      

      s.addEventListener('touchstart',forcedStopSignal, false);

  };
})

.factory('mySocket', function (socketFactory,PI_URI) {
 // console.log(PI_URI);
  
  var config = {
    prefix: '',//otherwise the default prefix is "socket:"
    ioSocket: io.connect(PI_URI)
  };

  return socketFactory(config);




  // var myIoSocket = io.connect('/some/path');

  // mySocket = socketFactory({
  //   ioSocket: myIoSocket
  // });

  return mySocket;


})

// .directive('dipesh', function() {
    
//     return function(scope, element, attrs) {
//       console.log(element);
//       var ele = document.getElementById('id1');
//       window.ele = ele;
//         ele.addEventListener("touchstart", function(){
//           alert('started');
//         }, false);

//         element.bind('keydown', function() {

//           element[0].innerText = "Dipesh";

//             //this.select();
//             // alert('cl');
//             console.log(element);
//             this.text = 'dipesh';
//           console.log('down');

//         });
//         element.onkeyup = function(){
//           alert('keyup')
//         }
//         element.bind('onkeyup', function() {
//           console.log('keyup');
//           element[0].innerText = "anil";

//             //this.select();
//             // alert('cl');
//             console.log(element);
//             this.text = 'dipesh';

//         });

//         element.bind('click', function() {

//           element[0].innerText = "Dipesh";

//             //this.select();
//             // alert('cl');
//             console.log(element);
//             this.text = 'clicked';

//         });

//     };
// })

.controller('mainCtrl',function($scope,mySocket){

  $scope.connected = false;

  window._socket = mySocket;
  mySocket.on('disconnect', function() {
      $scope.connected = false;
      //console.log('client socketio disconnect!')
  });

  mySocket.on('connect', function(){
    $scope.connected = true;
  });


  $scope.left = 5;
  $scope.right = 5;

  $scope.leftPlus = function(){
    ++$scope.left;
  }

  $scope.leftMinus = function(){
    --$scope.left;
  }

  $scope.rightPlus = function(){
    ++$scope.right;
  }

  $scope.rightMinus = function(){
    --$scope.right;
  }



})